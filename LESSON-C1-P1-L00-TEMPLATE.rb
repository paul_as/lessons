#!/usr/bin/env ruby

require 'optparse'

puts
puts "Программа выводит порядковый номер дня с начала года для введенной даты"
puts "пример: #{__FILE__} -d 1 -m 1 -y 2000"
puts
# получаем исходные данные из параметров запуска
params = {:d => nil, :m => nil, :y => nil}
OptionParser.new do |opt|
  opt.on('-d', '--day DAY', 'day of month')       { |o| params[:d] = o.to_i }
  opt.on('-m', '--month MONTH', 'month of year')  { |o| params[:m] = o.to_i }
  opt.on('-y', '--year YEAR', 'year')             { |o| params[:y] = o.to_i }
end.parse!
# https://stackoverflow.com/questions/26434923/parse-command-line-arguments-in-a-ruby-script
# args = Hash[ ARGV.join(' ').scan(/--?([^=\s]+)(?:=(\S+))?/) ]
# args = Hash[ ARGV.flat_map{|s| s.scan(/--?([^=\s]+)(?:=(\S+))?/) } ]
# Called with -x=foo -h --jim=jam it returns {"x"=>"foo", "h"=>nil, "jim"=>"jam"} so you can do things like:
# puts args['jim'] if args.key?('h')
#
#params = ARGV.getopts("ab:", "foo", "bar:")
# # params[:a] = true   # -a
# # params[:b] = "1"    # -b1
# # params[:foo] = "1"  # --foo
# # params[:bar] = "x"  # --bar x

# # получаем исходные данные из параметров запуска
# values = []
# values = ARGV[0..2].map(&:to_f)

# SEE; https://github.com/leejarvis/slop



# получаем исходные данные из консоли, если нет в параметрах
{'число' => :d, 'месяц' => :m, 'год' => :y}.each { |p|
  unless params[p[1]]
    puts
    print "введите #{p[0]}: "
    params[p[1]] = STDIN.gets.chomp.to_i
  end
}


puts
puts "\033[1;31m Работаем со следующими параметрами: #{params} \033[0m\n"
# https://stackoverflow.com/questions/2616906/how-do-i-output-coloured-text-to-a-linux-terminal
#          foreground background
# black        30         40
# red          31         41
# green        32         42
# yellow       33         43
# blue         34         44
# magenta      35         45
# cyan         36         46
# white        37         47
# https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
# You can play with different color: for i in {30..37}; do echo -e "\033[1;$i""m colorful text\033[0m"; done
# And different styles: for i in {1..7}; do echo -e "\033[$i;31""m different style\033[0m"; done

require 'pry'; binding.pry


puts
puts

