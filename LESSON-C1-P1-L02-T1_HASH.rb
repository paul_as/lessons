#!/usr/bin/env ruby


=begin
  Сделать хэш, содержащий месяцы с количеством дней в месяце,
  в цикле выводить те, в которых 30 дней
=end


puts
puts "Программа для вывода месяцев с 30 днями и всех месяцев в году"
puts "пример: #{__FILE__}"
puts


months = {
						:jan => { caption: "январь", size: 31 },
						:feb => { caption: "февраль", size: 28 },
						:mar => { caption: "март", size: 31 },
						:apr => { caption: "апрель", size: 30 },
						:may => { caption: "май", size: 31 },
						:jun => { caption: "июнь", size: 30 },
						:jul => { caption: "июль", size: 31 },
						:aug => { caption: "август", size: 31 },
						:sep => { caption: "сентябрь", size: 30 },
						:oct => { caption: "октябрь", size: 31 },
						:nov => { caption: "ноябрь", size: 30 },
						:dec => { caption: "декарь", size: 31 }
				}

	months.each do |m|
		puts m[1][:caption] if m[1][:size] == 30
	end


puts
puts

