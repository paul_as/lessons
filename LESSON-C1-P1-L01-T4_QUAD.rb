#!/usr/bin/env ruby

=begin
  Запрашиваем коэффициенты
  Выводим решение
=end

#require 'optparse'

puts
puts "Программа для решения квадратного уравнения"
puts "пример: #{__FILE__} 1 5 -6"



# получаем исходные данные из параметров запуска
values = []
values = ARGV[0..2].map(&:to_f)


# получаем исходные данные из консоли, если нет в параметрах
(0..2).each { |i|
  unless values[i]
    puts
    print "введите коэффициент #{i}: "
    values << STDIN.gets.chomp.to_f
  end
}


puts
puts "Квадратное уравнение вида ax²+bx+c=0 с коэффициентами a, b, c = #{values}"

discriminant_f = (values[1]**2)-4*values[0]*values[2]
puts "Дискриминант (D=b²-4ac) D=#{discriminant_f}"

puts "x=(-b±√D)/2a"
if discriminant_f > 0
  puts "D>0, уравнение имеет 2 корня"
  puts "(+√D) x=#{(-values[1]+Math.sqrt(discriminant_f))/2*values[0]}"
  puts "(-√D) x=#{(-values[1]-Math.sqrt(discriminant_f))/2*values[0]}"
elsif discriminant_f == 0
  puts "D=0, уравнение имеет 1 корень"
  puts "(±√D) x=#{(-values[1]+Math.sqrt(discriminant_f))/2*values[0]}"
  #puts "(-√D) x=#{(-values[1]-Math.sqrt(discriminant_f))/2*values[0]}"
elsif discriminant_f < 0
  puts "D<0, уравнение не имеет корней"
end



# http://www.bolshoyvopros.ru/questions/2883580-kakie-mozhno-privesti-primery-kvadratnogo-uravnenija.html#answer8489154
# Пример квадратного уравнения,
# которое традиционно записывают именно в таком виде:
# >>> ax²+bx+c=0,
# а, b, c, это у нас коэффициенты,
# х у нас здесь это неизвестное,
# которое находят по следующей формуле:
# >>> x=(-b±√D)/2a,
# >>> D=b²-4ac

# http://www.bolshoyvopros.ru/questions/299829-kak-sostavit-algoritm-reshenija-kvadratnogo-uravnenija.html#answer919750
# СХЕМА

#http://www.bolshoyvopros.ru/questions/991811-kak-reshit-uravnenie-s-kvadratnymi-kornjami.html#answer2850192
#Уравнение вида aх^2+bx+c решается Через дискрименант Д он равен b^2-4a*с... Если он больше нуля то X1= (-b+sprt(D))/2a; x2= (-b -sprt(D))/2a. Если равен нулю X1 = x2 = -b/2a. А если меньши нуля, то корней нет.

# require 'pry'; binding.pry

puts
puts

