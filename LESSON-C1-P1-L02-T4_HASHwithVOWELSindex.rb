#!/usr/bin/env ruby

=begin
  Заполнить хэш индексами гласных букв
=end

puts
puts "Программа для вывода хэша с индексами гласных букв"
puts "пример: #{__FILE__}"


#require 'pry'; binding.pry

hash = {}
vowels = %w(E Y U I O A)

('A'..'Z').each_with_index do |c, i|
  hash[c] = i+1 if vowels.include?(c)
end

puts hash

puts
puts

