#!/usr/bin/env ruby

puts "Программа расчет чека на кассе для введенных товарных позиций"


goods = {}; good = ""
loop do
  print "введите наименование товара: "
  good = STDIN.gets.chomp
  break if good == "стоп"

  print "введите цену товара: "
  price = STDIN.gets.chomp.to_f

  print "введите количество товара: "
  amount = STDIN.gets.chomp.to_f

  goods.merge!({ good => { price: price, amount: amount }})
end
# goods = {
#     "Творог"=>{:price=>80.0, :amount=>0.4},
#     "Масло"=>{:price=>120.0, :amount=>0.5},
#     "Сметана"=>{:price=>100.0, :amount=>0.5}
# }

puts "Работаем со следующими входными данными: #{goods}"
puts

goods = goods.map do |i|
  [
    i[0],
    {
      :price=>i[1][:price],
      :amount=>i[1][:amount],
      :cost=>i[1][:price]*i[1][:amount]
    }
  ]
end.to_h

puts "Позиции со стоимостью:"
puts goods

puts "Общая стоимость по чеку: #{goods.map { |i| i[1][:cost] }.inject(0, &:+)}"

# require 'pry'; binding.pry


