#!/usr/bin/env ruby

=begin
  Запрашиваем стороны треугольника
  Выводим площадь = 1/2*a*h
=end

#require 'optparse'
#require 'pry'

puts "Программа для расчета площади треугольника"
puts "пример: #{__FILE__} 10 10"


# получаем исходные данные из параметров запуска
a = ARGV[0].to_f if ARGV[0]
h = ARGV[1].to_f if ARGV[1]

# получаем исходные данные из консоли
unless a
  print "введите длинну основания: "
  a = STDIN.gets.chomp.to_f
end

unless h
  print "введите высоту: "
  h = STDIN.gets.chomp.to_f
end

puts 0.5*a*h
