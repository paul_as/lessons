#!/usr/bin/env ruby

# Приложение для управления Ж/Д станцией

# Ж/Д Станция
#   имеет:
#     название(@caption), указываемое при создании(#initialize)
#   может:
#     Принять(#take) поезд(Train), один за раз
#     Возвращать список(#get_current_trains) всех поездов на станции в данный момент
#       возвращать список по типу: грузовых, пассажирских
#     Отправлять(#depart) поезд(Train), один за раз
#
class Station
  attr_reader :caption

  # Создать станцию
  #
  # @param caption [String, #read] название создаваемой станции
  # @return [Station]
  def initialize(caption)
    @trains = []
    @caption = caption
    self
  end

  # Принять поезд
  #
  # @param train [Train, #read] экземпляр принимаемого поезда
  # @return [Array<Train>] массив поездов на станции, включая принятый
  def take(train)
    # @todo add DRY for all 'accepts only Train object instance in parameter'
    if not train.is_a?(Train)
      raise TypeError.new "Station::take(train) может принимать только экземпляр класса Train"
    end
    @trains.push(train)
  end

  # Отправить поезд
  #
  # @param train [Train, #read] отправляемый поезд
  # @return [Train, nil] убывший экземпляр Train, если не получилось nil
  def depart(train)
    # @todo add DRY for all 'accepts only Train object instance in parameter'
    if not train.is_a?(Train)
      raise TypeError.new "Station::depart(train) может отправлять только экземпляр класса Train"
    end
    @trains.delete(train)
  end

  # Список поездов на станции в данный момент
  #
  # @param type [Symbol<:any, :cargo, :passenger>, w/o parameter, #read] тип поездов(а)
  # @return [Array<Train>] список поездов на станции
  def get_current_trains(type = :any)
    case type
    when :any
      @trains
    when TRAIN_TYPE_CARGO
      @trains.detect { |train| train.type == TRAIN_TYPE_CARGO }
    when TRAIN_TYPE_PASSENGER
      @trains.detect { |train| train.type == TRAIN_TYPE_PASSENGER }
    else
      raise TypeError.new "Train::get_current_trains(type) запрошен поиск поезда неизвестного типа,
                           возможны следующие: #{TRAIN_TYPES}"
    end
  end

end


TRAIN_TYPE_CARGO     = :tt_carg
TRAIN_TYPE_PASSENGER = :tt_psgr

TRAIN_TYPES = [
    TRAIN_TYPE_CARGO,
    TRAIN_TYPE_PASSENGER
]

# Поезд @todo
class Train

  # Создать новый Поезд
  #
  # @param type [Symbol<TRAIN_TYPE_CARGO, TRAIN_TYPE_PASSENGER>, #read]
  # @return [Train]
  def initialize(type)
    raise RuntimeError.new "Train::initialize(type) можно создать поезд следующих
                            типов: #{TRAIN_TYPES}" unless TRAIN_TYPES.include? type
    @type = type
    self
  end

  # Возвращает тип поезда
  #
  # @return [Symbol<TRAIN_TYPE_CARGO, TRAIN_TYPE_PASSENGER>]
  def type
    @type
  end

end

# Маршрут
#   имеет:
#     +начальную станцию (@stations[0]), указываемую при создании(#initialize)
#     +конечную станцию (@stations[-1]), указываемую при создании(#initialize)
#     +промежуточные станции (@stations[1..-2])
#   может:
#     промежуточные станции
#       - добавлять в список (#insert_station_before)
#       - удалять из списка (#remove_station)
#       + возвращать список (#stations)
class Route

  # Создать новый Маршрут
  #
  # @param start_station [Station, #read]
  # @param end_station [Station, #read]
  #
  # @return [Route]
  def initialize(start_station, end_station)
    unless start_station.is_a?(Station) && end_station.is_a?(Station)
      raise TypeError.new "Route::initialize(start_station, end_station) можно создать маршрут для станций(Station)"
    end
    unless start_station.caption != end_station.caption
      raise ArgumentError.new "Route::initialize(start_station, end_station)
                               нельзя создать маршрут для станций(Station) с одинаковым названием(Station::caption)"
    end

    @stations = [start_station, end_station]
    self
  end


  # Вставить промежуточную станцию в списк остановок
  #
  # @param insert_station [Station, #read] станция, которую вставляем
  # @param before_station [Station, #read] станция, перед которой нужно вставить остановку
  # @return [Array<Station>] итоговый список(Array) станций(Station) в маршруте(Route)
  def insert_station_before(insert_station, before_station)
    before_station_index = @stations.index(before_station)
    if before_station_index > 0 # нельзя вставить перед начальной станцией, index должен быть больше 0
      @stations.insert(before_station_index, insert_station)
    else
      raise ArgumentError.new "Route::insert_station_before(before_station, insert_station) ошибка данных в параметре,
                               станция(Station) отсутствует в списке, либо указана начальная."
    end
  end

  # Удалить станцию из списка остановок
  #
  # @param station [Station, #read]
  # @return [Station<station>, nil]
  def remove_station(station)
    @stations.delete(station)
  end

  # Список станций по маршруту следования
  #
  # @return [Array<Station>]
  attr_reader :stations
  # def stations
  # end


end

############################################################################################
############################################################################################
############################################################################################
# @todo Split into files!!!
# require 'pry'; binding.pry

puts
puts

stn_nvr = Station.new('Новороссийск')
stn_krd = Station.new('Краснодар')
stn_rst = Station.new('Ростов')
stn_vrn = Station.new('Воронеж')
stn_msk = Station.new('Москва')



# exception
# require 'pry'; binding.pry
# puts stn_msk.take(:notTrain).inspect

rou_nvr_msk = Route.new(stn_nvr, stn_msk)

# exception
# require 'pry'; binding.pry
# stnKrd2 = Station.new('Краснодар')
# rouMskKrdWrong1 = Route.new(stnKrd, stnKrd2)
# rouMskKrdWrong2 = Route.new(:stnKrd, stnKrd2)
# rouMskKrdWrong3 = Route.new(stnKrd, :stnKrd2)
# rouMskKrdWrong4 = Route.new(:stnKrd, :stnKrd2)


puts rou_nvr_msk.insert_station_before(stn_rst, stn_msk).inspect
puts
puts rou_nvr_msk.insert_station_before(stn_krd, stn_rst).inspect
puts
puts rou_nvr_msk.insert_station_before(stn_vrn, stn_msk).inspect
puts

puts rou_nvr_msk.stations.inspect

require 'pry'; binding.pry

puts rou_nvr_msk.remove_station(stn_rst).inspect
puts
puts rou_nvr_msk.remove_station(stn_krd).inspect
puts
puts rou_nvr_msk.remove_station(stn_vrn).inspect



puts
puts
# require 'pry'; binding.pry
exit










trnCarg = Train.new(TRAIN_TYPE_CARGO)
trnPass = Train.new(TRAIN_TYPE_PASSENGER)
# exception
# require 'pry'; binding.pry
# trnWrong = Train.new(:oops)


puts 'Take cargo'
puts stn_msk.take(trnCarg).inspect
puts
puts 'Take passngr'
puts stn_msk.take(trnPass).inspect
puts
puts

puts 'stn_msk.get_current_trains.inspect'
puts stn_msk.get_current_trains.inspect
puts
puts 'stn_msk.get_current_trains(:any).inspect'
puts stn_msk.get_current_trains(:any).inspect
puts
puts 'stn_msk.get_current_trains(TRAIN_TYPE_CARGO).inspect'
puts stn_msk.get_current_trains(TRAIN_TYPE_CARGO).inspect
puts
puts 'stn_msk.get_current_trains(TRAIN_TYPE_PASSENGER).inspect'
puts stn_msk.get_current_trains(TRAIN_TYPE_PASSENGER).inspect
puts



























# require 'optparse'
#
# puts
# puts "Программа выводит порядковый номер дня с начала года для введенной даты"
# puts "пример: #{__FILE__} -d 1 -m 1 -y 2000"
# puts
# # получаем исходные данные из параметров запуска
# params = {:d => nil, :m => nil, :y => nil}
# OptionParser.new do |opt|
#   opt.on('-d', '--day DAY', 'day of month')       { |o| params[:d] = o.to_i }
#   opt.on('-m', '--month MONTH', 'month of year')  { |o| params[:m] = o.to_i }
#   opt.on('-y', '--year YEAR', 'year')             { |o| params[:y] = o.to_i }
# end.parse!
# # https://stackoverflow.com/questions/26434923/parse-command-line-arguments-in-a-ruby-script
# # args = Hash[ ARGV.join(' ').scan(/--?([^=\s]+)(?:=(\S+))?/) ]
# # args = Hash[ ARGV.flat_map{|s| s.scan(/--?([^=\s]+)(?:=(\S+))?/) } ]
# # Called with -x=foo -h --jim=jam it returns {"x"=>"foo", "h"=>nil, "jim"=>"jam"} so you can do things like:
# # puts args['jim'] if args.key?('h')
# #
# #params = ARGV.getopts("ab:", "foo", "bar:")
# # # params[:a] = true   # -a
# # # params[:b] = "1"    # -b1
# # # params[:foo] = "1"  # --foo
# # # params[:bar] = "x"  # --bar x
#
# # # получаем исходные данные из параметров запуска
# # values = []
# # values = ARGV[0..2].map(&:to_f)
#
# # SEE; https://github.com/leejarvis/slop
#
#
#
# # получаем исходные данные из консоли, если нет в параметрах
# {'число' => :d, 'месяц' => :m, 'год' => :y}.each { |p|
#   unless params[p[1]]
#     puts
#     print "введите #{p[0]}: "
#     params[p[1]] = STDIN.gets.chomp.to_i
#   end
# }
#
#
# puts
# puts "\033[1;31m Работаем со следующими параметрами: #{params} \033[0m\n"
# # https://stackoverflow.com/questions/2616906/how-do-i-output-coloured-text-to-a-linux-terminal
# #          foreground background
# # black        30         40
# # red          31         41
# # green        32         42
# # yellow       33         43
# # blue         34         44
# # magenta      35         45
# # cyan         36         46
# # white        37         47
# # https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
# # You can play with different color: for i in {30..37}; do echo -e "\033[1;$i""m colorful text\033[0m"; done
# # And different styles: for i in {1..7}; do echo -e "\033[$i;31""m different style\033[0m"; done
