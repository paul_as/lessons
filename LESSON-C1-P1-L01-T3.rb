#!/usr/bin/env ruby

=begin
  Запрашиваем стороны треугольника
  Выводим тип треугольника
=end

#require 'optparse'

puts "Программа для определения типа треугольника"
puts "пример: #{__FILE__} 10 8 6"
puts



# получаем исходные данные из параметров запуска
values = []
values = ARGV[0..2].map(&:to_f)


# получаем исходные данные из консоли, если нет в параметрах
(0..2).each { |i|
  unless values[i]
    print "введите сторону #{i}: "
    values << STDIN.gets.chomp.to_f
  end
}


print "Треугольник со сторонами: #{values}, "

if values[0] == values[1] && values[1] == values[2]
  # равносторонний все 3 стороны равны
  print " равносторонний, равнобедренный"
  
elsif values[0] == values[1] || values[1] == values[2] || values[2] == values[0]
  # равнобедренный 2 стороны равны
  print " равносторонний"
  
else
  print "не равносторонний, не равнобедренный"
  
end

hipotinuza_f = values.max
values.delete_at(values.index(hipotinuza_f))

# Условие прямоугольности треугольника: катет1**2 + катет2**2 == гипотенуза**2
if hipotinuza_f**2 == values.map{ |v| v**2 }.inject(0, :+)
  print ", прямоугольный"
else
  print ", не прямоугольный"
end

puts
puts
#require 'pry'; binding.pry


