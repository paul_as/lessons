#!/usr/bin/env ruby

=begin
  Заполнить массив числами Фибоначчи до 100
=end

#require 'optparse'

puts
puts "Программа для вывода массива чисел Фибоначчи до 100"
puts "пример: #{__FILE__}"


require 'pry'; binding.pry

maximum = 100
fibs = [0, 1]
n=2
loop do
  fibo = fibs[n-1] + fibs[n-2]
  n += 1

  break if fibo > maximum
  fibs << fibo
end

puts fibs

puts
puts

