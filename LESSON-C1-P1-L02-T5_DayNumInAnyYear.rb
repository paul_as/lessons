#!/usr/bin/env ruby

require 'optparse'

puts "Программа выводит порядковый номер дня с начала года для введенной даты"
puts "пример: #{__FILE__} -d 1 -m 1 -y 2000"
# получаем исходные данные из параметров запуска
params = {:d => nil, :m => nil, :y => nil}
OptionParser.new do |opt|
  opt.on('-d', '--day DAY', 'day of month')       { |o| params[:d] = o.to_i }
  opt.on('-m', '--month MONTH', 'month of year')  { |o| params[:m] = o.to_i }
  opt.on('-y', '--year YEAR', 'year')             { |o| params[:y] = o.to_i }
end.parse!

# получаем исходные данные из консоли, если нет в параметрах
{'число' => :d, 'месяц' => :m, 'год' => :y}.each { |p|
  unless params[p[1]]
    print "введите #{p[0]}: "
    params[p[1]] = STDIN.gets.chomp.to_i
  end
}

puts "Работаем со следующими параметрами: #{params}"



# # # год, номер которого кратен 400, — високосный;
# # # остальные годы, номер которых кратен 100, — невисокосные (например, го­ды 1700, 1800, 1900, 2100);
# # # остальные годы, номер которых кратен 4, — високосные[5].
# # if    params[:y]%400==0; puts "\033[1;34mВисокосный    (%400 #{params[:y]%400})\033[0m\n"
# # elsif params[:y]%100==0; puts "\033[1;31mНЕ Високосный (%100 #{params[:y]%100})\033[0m\n"
# # elsif params[:y]%4==0;   puts "\033[1;34mВисокосный    (%4 #{params[:y]%4})\033[0m\n"
# # else;                    puts "\033[1;31mНЕ Високосный (прочее)\033[0m\n"
# # end
# 
# # Високосный год определяется по следующему правилу:
# # Год високосный,
# #   если он делится на четыре без остатка,
# #     но если он делится на 100 без остатка, это не високосный год.
# # Однако, если он делится без остатка на 400, это високосный год.
# print "\033[1;33m#{params[:y]} \033[0m"
# if params[:y]%4==0 && params[:y]%100!=0 || params[:y]%400==0; puts "\033[1;31mВисокосный\033[0m\n"
# else; puts "\033[1;34mНЕ Високосный\033[0m"; end

months_days = [31,28,31,30,31,30,31,31,30,31,30,31]
months_days[1] = 29 if params[:y]%4==0 && params[:y]%100!=0 || params[:y]%400==0


#require 'pry'; binding.pry
#puts params[:d] < 2 ? params[:m] : months_days[0..params[:m]-2].inject(0, &:+) + params[:d]

if params[:m] < 2
  puts params[:d]
else
  puts months_days[0..params[:m]-2].inject(0, &:+) + params[:d]
end





