#!/usr/bin/env ruby

=begin
  Заполнить массив числами от 10 до 100 с шагом 5
=end


puts
puts "Программа для вывода чисел от 10 до 100 с шагом 5"
puts "пример: #{__FILE__}"



require 'pry'; binding.pry


# array = (10..100).step(5).to_a
array = (10..100).map {|n| n if n % 5 == 0 }.to_a.compact
puts array


puts
puts

