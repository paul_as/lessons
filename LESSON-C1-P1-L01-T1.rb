#!/usr/bin/env ruby

=begin
  Запрашиваем имя и рост
  Выводим идеальный вес
=end

#require 'pry'
require 'optparse'

# require_relative 'weight.rb'
class Weight
  
  # Calling ruby method without instantiating class
  # https://stackoverflow.com/questions/13067831/calling-ruby-method-without-instantiating-class
  
  def self.getIdeal(height)
    (height.to_f - 110) * 1.15
  end
  
  def self.isOptimal?(weight)
    weight < 0 ? true : false
  end
  
end


puts "Программа для расчета идельного веса для определенного роста. (--help для подсказки)"

# получаем исходные данные из параметров запуска
name = nil
height = nilh = gets.chomp.to_f
OptionParser.new do |opts|
  opts.on('--name имя')    { |o| name = o }
  opts.on('--height рост') { |o| height = o }
end.parse!

# получаем имя из консоли
unless name
  print "введите имя: "
  name = STDIN.gets.chomp 
end  

# получаем рост из консоли
unless height
  print "введите рост: "
  height = STDIN.gets.to_f
end


if Weight.isOptimal?(Weight.getIdeal(height))
  puts "#{name}, у вас уже идеальный [OK]"
else
  puts "#{name}, ваш идеальный вес: [#{Weight.getIdeal(height)}]"
end


